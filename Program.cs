﻿using System.Collections.Generic;
using System;

namespace _61160044
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = 10;
            string name = "Waratchapon Ponpiya";
            bool True = true;
            char symbol = 'X';
            double salary = 15000.00;

            //Array
            int[] arr = new int[10];
            arr[0] = 1;

            //ArrayList
            List<int> list = new List<int>();
            list.Add(1);

            Console.WriteLine("Hello World!" + " " + name + (22 + 22));

            //Intro if else
            int point = 85;
            if (point > 80)
            {
                Console.WriteLine("A");
            }
            else if (point <= 80)
            {
                Console.WriteLine("F");
            }
            else
            {
                Console.WriteLine("Error");
            }

            int[] arr1 = new int[] { 1, 2, 3, 4, 5 };
            for (int i = 0; i < arr1.Length; i++)
            {
                Console.Write(arr1[i] + " ");
            }

            // Calculater Rectangular area
            int width = 10;
            int length = 10;
            Console.WriteLine("Width : " + width + " " + "Length : " + width);
            Console.WriteLine("Result : " + width * length);

            //Calculater Triangle area
            int width = 15;
            int length = 15;
            Console.WriteLine("Calculater Triangle area");
            Console.WriteLine("Width : " + width + " " + "Length : " + width);
            Console.WriteLine("Result : " + (width * length) / 2);


            // Calculater Average
            double[] arr1 = new double[] { 1, 2, 3, 4, 5 };
            Console.WriteLine("Result : " + Average(arr1));

            double[] arr2 = new double[] { 3, 9, 15, 19, 21 };
            Console.WriteLine("Result : " + Average(arr2));

            double[] arr3 = new double[] { 12.5, 10, 14.5, 16 };
            Console.WriteLine("Result : " + Average(arr3));

            // Calculater Minvalue
            int[] arr4 = new int[] { 1, 2, 3, 4, 5 };
            Console.WriteLine("Result : " + Min(arr4));

            int[] arr5 = new int[] { 4, -1, 7, 0, 5 };
            Console.WriteLine("Result : " + Min(arr5));

            int[] arr6 = new int[] { 10, 7, 15, 12, 99 };
            Console.WriteLine("Result : " + Min(arr6));

            //Find EvenandOdd
            int[] arr7 = new int[] { 10, 11, 12, 13, 14, 15, 16 };
            Console.WriteLine("Result Even : " + Even(arr7));
            Console.WriteLine("Result Odd : " + Odd(arr7));

            // Find index
            int[] arr8 = new int[] { 1, 2, 3, 4, 5 };
            int number = 4;

            for (int i = 0; i < arr8.Length; i++)
            {
                if (number == arr8[i])
                {
                    Console.WriteLine("Result : " + i);
                    break;
                }

            }

            // Find hour and minute
            int hour1 = 1;
            int minute1 = 19;
            int hour2 = 0;
            int minute2 = 40;
            int totalminute1 = ((minute1 + minute2) / 60);
            int totalminute2 = ((minute1 + minute2) % 60);

            int totalhour = (hour1 + hour2 + totalminute1);
            Console.WriteLine("Result : " + totalhour + " hour " + totalminute2 + " minute");

            int[] arr9 = new int[] { 2, 1, 5, 6, 7 };
            int[] arr10 = new int[] { 4, 3, 7, 5, 9 };
            List<int> list = new List<int>();
            for (int i = 0; i < arr9.Length; i++)
            {
                for (int j = 0; j < arr10.Length; j++)
                {
                    if (arr9[i] == arr10[j])
                    {
                        list.Add(arr9[i]);
                    }

                }

            }



            Console.Write("Result : ");
            for (int k = 0; k < list.Count; k++)
            {
                if (k == list.Count - 1)
                {
                    Console.Write(list[k]);
                    continue;
                }
                else
                {
                    Console.Write(list[k] + ",");
                }
            }


            String sentence = "1112111";
            char[] charArr = sentence.ToCharArray();
            int tempslice = charArr.Length / 2;
            if (tempslice % 2 != 0)
            {


            }
            Console.WriteLine(charArr.Length / 2);

            Console.WriteLine(charArr[1]);
            foreach (char ch in charArr)
            {
                Console.WriteLine(ch);
            }

            String list = "ABAC";
            char[] charArr1 = list.ToCharArray();
            for (int i = 0; i < charArr1.Length; i++)
            {
                for (int j = 1; j < charArr1.Length; j++)
                {
                    if (charArr1[i] == charArr1[j])
                    {
                        Console.WriteLine(charArr1[i] + " " + charArr1[j]);
                        Console.WriteLine("1");

                    }

                }
            }

            double km = 10;
            double pricetaxi = 0;
            double totalprice = 0;


            if (km > 0)
            {
                totalprice += 35;
            }
            if (km > 1)
            {
                if (km - 1 > 9)
                {
                    totalprice += (9 * 5.5);

                }
                else
                {
                    totalprice += (km - 1) * 5.5;
                }
            }
            if (km > 10)
            {
                if (km - 10 > 9)
                {
                    totalprice += (9 * 6.5);
                }
                else
                {
                    totalprice += (km - 10) * 6.5;
                }
            }
            if (km > 20)
            {
                if (km - 20 > 19)
                {
                    totalprice += (19 * 7.5);
                }
                else
                {
                    totalprice += (km - 20) * 7.5;
                }
            }
            if (km > 40)
            {
                totalprice += (km - 40) * 10;

            }

            Console.WriteLine(totalprice);

        }


        static double Average(double[] arr)
        {
            double sum = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                sum = sum + arr[i];
            }
            double result = sum / arr.Length;
            return result;
        }

        static int Min(int[] arr)
        {
            int min = Int32.MaxValue;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] < min)
                {
                    min = arr[i];
                }

            }
            return min;
        }

        static int Even(int[] arr)
        {
            int countEven = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] % 2 == 0)
                {
                    countEven++;
                }
            }
            return countEven;
        }

        static int Odd(int[] arr)
        {
            int countOdd = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] % 2 != 0)
                {
                    countOdd++;
                }

            }
            return countOdd;
        }

    }
}
